in arne_poortinga.py staan voorbeeldantwoorden voor code.

Algemeen:
    als niet aan randvoorwaarde voldaan wordt 0 pnt
    als code niet correct werkt 0 pnt
    als code voor een vraag een syntaxfout geeft wordt de vraag verder niet beoordeeld.
    aangegeven aantallen punten zijn maxima. Als uitleg onzinnig is o.i.d. kan dit lager zijn

# Vraag 1 (5 pnt):
antwoord: O(1)
          want er is geen enkele loop die van de input afhangt

becijfering: 
        juist antwoord 2 pnt
        uitleg klopt met antwoord en is 'logisch' 3 pnt

#vraag 2
antwoord: RadixSort. Dit loopt in O(k*n) waarbij k de grootste lengte van een getal in de array is. De andere algoritmen lopen op z'n best in O(nlog(n))


becijfering:
    5 voor code 10 voor uitleg (telt voor tijdscomplexiteit)

    uitleg:
    3 voor correct noemen/'uitleg' O() QS
    5 voor correct noemen/'uitleg' O() BS
    2 voor logische conclusie
    als bij uitleg beweerd wordt dat QS sneller loopt dan BS: max 1 pnt voor analyse QS

    code:
    als werkt volgens spec 5 pnt


#vraag 3 (15):
antwoord: gebruik queue meest geschikt voor buffer omdat volgorde bewaard blijft, maar andere structuren mogen ook maar vragen meer werk

becijfering:
   Totaal 15 pnt waarvan
          8 voor de keuze van de meest efficiënte datastructuur en algoritme
          7 voor werkende code (ook als dit niet het meest efficiënt is), 
          als geen overerving gebruikt wordt -2 pnt

vraag 4
           totaal 15 punten waarvan 7 voor a en 8 voor b

		gegeven een lijst met integers: [12, 7, 23, 9, 14, 8, 6, 13]
		vraag a:       12
					/     \
                   7       23
                  / \      / 
                 6   9    14
                    /     /
                   8     13

		vraag b: vullen en dan in-order traverse
					als naam traverse niet genoemd, maar wel correct beschreven -2


vraag 5
        stappen:
			sorteer lijstje!! mag met sort()
			binair zoeken

becijfering:
    7pnt voor uitleg: niet efficient; sorteren loopt in O(n2) tot O(nlogn) er doorheen lopen in O(n)
    8 pnt voor werkende code


vraag 6:
load factor 3/9 ^= 30%
het is het aantal gevulde cellen gedeeld door het totaal aantal beschibare cellen. Als dit naar 1 nadert dan krijg je veel collisions waardoor efficientie van de hashmap afneemt (naar O(n) nadert ipv O(1)

2 voor juist antwoord
8 voor uitleg, als O() niet of fout gebruikt -4

vraag 7:
        maximaal aantal punten: 15
            waarvan 9 punten als de voor recursiviteit  benodigde stappen (3 pnt voor elk) aanwezig zijn

pylint3 score >= 7: dan 5 punten


