#!/usr/bin/env python3

'''Proefentamen algoritmen en datastructuren

Het tentamen bestaat uit het aanvullen van code c.q. het invullen van variabelen
in dit script en het schrijven van een module.
In totaal moet je zeven vragen beantwoorden.

Instructies:
    • Hernoem het bestand voornaam_achternaam.py. Vervang voornaam door je
      voornaam en achternaam door je achternaam. Eventuele tussenvoegsels laat
      je weg. Ik zelf zou bijvoorbeeld het bestand moeten hernoemen naar
      arne_poortinga.py).
    • Vul vervolgens je voor- en achternaam, eventuele tussenvoegsels,
      studentnummer en de computer waar je achter werkt in op de aangegevenr
      plaatsen in dit bestand.
    • Alle documentatie is beschikbaar met behulp van pydoc3.
    • Zorg er voor dat de het script geen syntax-fouten bevat. Dit kun
       makkelijk doen in een terminal met behulp van pylint3.

      		pylint3 voornaam_achternaam.py


    • als je ingeleverde werk van pylint3 met een score >= 7 krijgt, krijg
      je hiervoor 5 punten.
    • als een functie een syntaxfout bevat wordt deze verder niet beoordeeld
    • als je functie niet aan de gestelde randvoorwaarden voldoet krijg je er
      geen punten voor
    • Licht je antwoord toe, voor de uitleg krijg je veruit de meeste punten!
    • Schroom niet om voor de uitleg ASCII-art of pseudocode te gebruiken als
      je denkt dat dat verhelderend zou kunnen werken.
    • Ga, als ik om de tijdscomplexiteit (is hetzelfde als O(), is hetzelfde
      als big-Oh()) vraag, van het worst-case scenario uit tenzij uitdrukkelijk
      iets anders gevraagd wordt of het algoritme/de datastructuur normaliter
      veel beter presteert dan in de worst case. Geef in dat geval beide O()
      met uitleg wanneer welke voorkomt.
    • Nogmaals: licht je antwoord toe!
    • Maak voordat je aan een volgende vraag begint steeds een back-up van je
      werk, dan verklein je de kans dat je alles kwijtraakt.
    • Voor de zekerheid staat er ook een voornaam_achternaam.py.bak op je
      desktop. Het bestand zelf is alleen lezen, maar de inhoud kun je
      gebruiken om opnieuw te beginnen als er fout gegaan is.

Inleveren:
    • Controleer je bestand een laatste maal met pylint3
    • Lever je bestand in via het submitscript “submit_your_work”. Geef commando
      “submit_your_work help” om te zien hoe dit werkt.

Cijfer:
Je cijfer wordt als volgt berekend:

    (aantal behaalde punten/maximaal te behalen punten)*10.

    Mocht je minder dan een 1 gescored hebben, dan wordt dit ‘afgerond’ tot een
    1. Je hebt een voldoende als je een 5.5 of hoger gescored hebt.
'''

import tentamen

VOORNAAM = ''
TUSSENVOEGSELS = ''
ACHTERNAAM = ''
STUDENTNUMMER = ''
COMPUTERNUMMER = ''


#====================================================================
#                     begin tentamenvragen
#====================================================================

def vraag01():
    '''vraag 1.
        5 pnt
       analyseer de code in functie01 (zie onder). Wat is de tijdscomplexiteit
       van deze functie uitgedrukt in O()?
       Zet je antwoord en je uitleg in de desbetreffende strings
    '''
    tijdscomplexiteit_functie01 = ''
    uitleg = '''vervang dit door je uitleg'''
    return (tijdscomplexiteit_functie01, uitleg)


def vraag02(lijstje):
    ''' vraag 2.
           totaal 15 pnt, waarvan 5 voor code en 10 voor de uitleg.

        Implementeer de functie volgens de onderstaande pseudocode.

        Doel: gegeven een lijst met n integers die zo snel mogelijk gesorteerd moet worden. Sorteer deze lijst
        Input: een lijst met integers 'lijstje'
        Output: een tupel met
            je uitleg waarom je voor een bepaalde functie gekozen hebt
            en een gesorteerde lijst met integers
        Randvoorwaarden: gebruik een sorteerfunctie uit de module tentamen
                         de uitleg aan de hand van de O() waarom je voor dit
                         algoritme gekozen hebt staat in de desbetreffende string.
    '''
    uitleg = '''zet hier je uitleg'''

    #zet voor de return statement jouw code
    gesorteerde_lijst = lijstje

    return (uitleg, gesorteerde_lijst)


def vraag03():
    '''vraag 3.
        Totaal 15 pnt waarvan
             8 voor de uitleg
             7 voor werkende code (ook als dit niet het meest efficiënt is)

        Doel: Bij grootschalige dataverwerking vormt de database server de bottleneck, daarom moet er een buffer geimplementeerd worden. De input moet op volgorde van binnenkomst verwerkt worden.
        Input: de invoer van de buffer ontvangt steeds een string genaamd inputstring
        Output: de uitvoer van de buffer is steeds 'insert into Dummy <inputstring>;' waarbij <inputstring> steeds vervangen wordt door een van de gebufferde strings. 
        Randvoorwaarden: gebruik een datastructuur uit de module tentamen
        volgorde van verwerking wordt niet door de buffer veranderd
        implementeer deze buffer in een apart bestand genaamd buffer.py
    '''
    uitleg = '''zet hier je uitleg'''

    return uitleg


def vraag04():
    '''vraag 4.
            totaal 15 punten waarvan 7 voor a en 8 voor b

		gegeven een lijst met integers: [12, 7, 23, 9, 14, 8, 6, 13]
		vraag a: teken in ascii-art hoe een binary search tree er uit ziet als je de waardes in deze lijst er in stopt
		vraag b: leg uit hoe je een binary search tree kunt gebruiken om te sorteren
		    
    '''
    antwoord_vraag_a = '''zet hier je ascii-art'''
    antwoord_vraag_b = '''zet hier je uitleg'''
    return (antwoord_vraag_a, antwoord_vraag_b)


def vraag05(lijstje, waarde):
    '''vraag 5.
        Totaal 15 punten waarvan
             7 voor de uitleg over efficientie
             8 voor werkende code (ook als dit niet het meest efficiënt is)

        Implementeer onderstaande pseudocode.

        Doel: bepaal of een waarde in een Python list zit, gebruik hiervoor binary search. Je mag alle standaard python functies gebruiken.
        Input: een python lijst 'lijstje' met integers, een integer 'waarde'
        Output: een tupel met:
					een boolean: True als waarde in lijstje zit, anders False
                    uitleg waarom dit wel of niet een efficiente manier is
        Randvoorwaarden: Gebruik binary search
    '''
    antwoord_efficientie = '''leg hier uit waarom dit wel/niet efficient is'''
    
    antwoord = ''
    #zet voor de return statement jouw code
    
    return (antwoord, antwoord_efficientie)


def vraag06():
    '''vraag 6.
        Totaal aantal punten maximaal 10 waarvan 2 voor het juiste antwoord
        
        stel je hebt een hash table waarvan de onderliggende datastructuur er als volgt uit ziet:
        
			[17|  |  |  |22|  |   |   |2]
			
		Wat is de 'load factor'?
		Leg ook uit wat de load factor is en waar om deze belangrijk is.
    '''
    antwoord = ''
    toelichting = '''zet hier je toelichting'''
    return (antwoord, toelichting)


def vraag07(lijstje):
    '''vraag 7. Implementeer onderstaande pseudocode
        maximaal aantal punten: 15
            waarvan 9 punten als de voor recursiviteit benodigde stappen aanwezig zijn
            6 punten als de code ook correct werkt

        Doel: bepaal RECURSIEF het product van twee integers
        Input: een twee integers (integer1 en integer2)
        Output: het product van integer1 en integer2
        Randvoorwaarden: de functie moet recursief zijn
    '''
    #schrijf hier je code, de print statement staat er alleen omdat pylint
    #anders puntenaftrek geeft. Deze statement mag je dus verwijderen.
    print(lijstje)


#====================================================================
#                      einde tentamenvragen
#     hier onder volgen de hulpfuncties voor vraag 1 en vraag 6
#====================================================================


def doeiets():
    '''hulpfunctie voor vraag 01'''
    print('n')


def functie01(l_met_ints):
    '''Analyseer de tijdscomplexteit van deze functie voor de lengte van de input
        input: lijst met integers l_met_ints
    '''
    integer = 5
    print('n')
    print(l_met_ints)
    doeiets()
