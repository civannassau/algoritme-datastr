#!/usr/bin/env python3

'''Tentamen algoritmen en datastructuren d.d. 2021-04-14
    Zie voor instructies en meer vragen het pdf bestand
'''

import tentamen


def vraag04(lijstje):
    '''
    Doel: geef de kleinste waarde in een lijst met integers terug
    input: een lijst met integers genaamd 'lijstje'
    output: het kleinste getal dat zich in 'lijstje' bevindt.
    precondities: gebruik een datastructuur uit de module tentamen
    postcondities: -

    Algoritme:
    '''
    #Schrijf hier je code


def vraag06(stringetje):
    '''
    Doel: bepaal of stringetje een palindroom is
    input: een string bestaande uit kleine letters genaamd 'stringetje'
    output: True als stringetje een palindroom is, False als dit niet zo is
    precondities: gebruik hiervoor een Dequeue
    postcondities: -

    Algoritme:
    '''
    #Schrijf hier je code


def vraag08(x, y):
    '''
    Doel: bepaal RECURSIEF x**y (x tot de macht y)
    Input: twee integers x en y
    Output: x**y
    Randvoorwaarden: de functie moet recursief zijn
    '''
    #schrijf hier je code


############## EINDE ##############################
