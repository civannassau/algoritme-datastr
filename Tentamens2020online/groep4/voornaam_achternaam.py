#!/usr/bin/env python3

'''Tentamen algoritmen en datastructuren d.d. 2020-05-06

Het tentamen bestaat uit het aanvullen van code c.q. het invullen van variabelen
in dit script.
In totaal moet je zeven functies (vraag01() … vraag07() ) veranderen of aanvullen.

Instructies:
    • Hernoem het bestand voornaam_achternaam.py. Vervang voornaam door je
      voornaam en achternaam door je achternaam. Eventuele tussenvoegsels laat
      je weg. Ik zelf zou bijvoorbeeld het bestand moeten hernoemen naar
      arne_poortinga.py).
    • Vul vervolgens je voor- en achternaam, eventuele tussenvoegsels,
      studentnummer en de computer waar je achter werkt in op de aangegevenr
      plaatsen in dit bestand.
    • Alle documentatie is beschikbaar met behulp van pydoc3. Andere
      documentatie is NIET toegestaan.
    • Zorg er voor dat de het script geen syntax-fouten bevat. Dit kun
       makkelijk doen in een terminal met behulp van pylint3.

      		pylint3 voornaam_achternaam.py

        Afhankelijk van de installatie op je computer moet je pylint3 vervangen door pylint.
    • als je ingeleverde werk van pylint3 met een score >= 7 krijgt, krijg
      je hiervoor 5 punten.
    • als een functie een syntaxfout bevat wordt deze verder niet beoordeeld
    • als je functie niet aan de gestelde randvoorwaarden voldoet krijg je er
      geen punten voor
    • Licht je antwoord toe, voor de uitleg krijg je veruit de meeste punten!
    • Schroom niet om voor de uitleg ASCII-art of pseudocode te gebruiken als
      je denkt dat dat verhelderend zou kunnen werken.
    • Ga, als ik om de tijdscomplexiteit (is hetzelfde als O(), is hetzelfde
      als big-Oh()) vraag, van het worst-case scenario uit tenzij uitdrukkelijk
      iets anders gevraagd wordt of het algoritme/de datastructuur normaliter
      veel beter presteert dan in de worst case. Geef in dat geval beide O()
      met uitleg wanneer welke voorkomt.
    • Nogmaals: licht je antwoord toe!
    • Maak voordat je aan een volgende vraag begint steeds een back-up van je
      werk, dan verklein je de kans dat je alles kwijtraakt.
    • Voor de zekerheid is er ook een voornaam_achternaam.py.bak bijgesloten.
      Het bestand zelf is alleen lezen, maar de inhoud kun je gebruiken om
      opnieuw te beginnen als er fout gegaan is.

Inleveren:
    • Let op! Je kunt maar één keer iets inleveren. Wees dus zorgvuldig!
    • Controleer je bestand een laatste maal met pylint3 (of pylint)
    • Zip je bestand en lever de zip-file in via de link op BlackBoard
    • Meld je af bij de surveillant
    • Let op de tijd: als de tijd verstreken is verdwijnt de inleverlink!
      Zorg dus dat je op tijd inlevert.

Cijfer:
Je cijfer wordt als volgt berekend:

    (aantal behaalde punten/maximaal te behalen punten)*10.

    Mocht je minder dan een 1 gescored hebben, dan wordt dit ‘afgerond’ tot een
    1. Je hebt een voldoende als je een 5.5 of hoger gescored hebt.
'''

import tentamen

GROEP = 4
VOORNAAM = ''
TUSSENVOEGSELS = ''
ACHTERNAAM = ''
STUDENTNUMMER = ''
COMPUTERNUMMER = ''


#====================================================================
#                     begin tentamenvragen
#====================================================================

def vraag01(lijstje):
    '''vraag 1.
        Totaal 15 pnt waarvan
             8 voor de keuze van de meest efficiënte datastructuur en algoritme
             7 voor werkende code (ook als dit niet het meest efficiënt is)

        Implementeer de pseudocode.

        Doel: Sorteer de inhoud van de input van groot naar klein
        Input: een python lijst met positieve integers 'lijstje'
        Output: een python lijst met dezelfde inhoud als de input, maar gesorteerd
            van groot naar klein.
        Randvoorwaarden: gebruik een datastructuur (dus géén functie!)
             uit de module tentamen
             Geen in Python ingebouwde sorteerfunctie gebruiken
    '''
    #zet voor de return statement jouw code om de pseudocode in deze functie te
    #implementeren

    return lijstje


def vraag02():
    '''vraag 2.
        5 pnt
       analyseer de code in functie01 (zie onder). Wat is de tijdscomplexiteit
       van deze functie uitgedrukt in O()?
       Zet je antwoord en je uitleg in de desbetreffende strings
    '''
    tijdscomplexiteit_functie01 = 'vervang dit door de juiste datastructuur'
    uitleg = '''vervang dit door je uitleg'''
    return (tijdscomplexiteit_functie01, uitleg)


def vraag03(lijstje):
    ''' vraag 3.
           totaal 15 pnt, waarvan 5 voor code en 10 voor de uitleg.

        Implementeer de functie volgens de onderstaande pseudocode.

        Doel: gegeven een lijst met n integers die hoogst waarschijnlijk al
            gesorteerd is, maar dit is niet zeker. Om zeker te maken dat de lijst
            gesorteerd is moet ze opnieuw gesorteerd worden. Sorteer deze lijst
        Input: een waarschijnlijk gesorteerde lijst met integers 'lijstje'
        Output: een tupel met
            je uitleg waarom je voor een bepaalde functie gekozen hebt
            en een gesorteerde lijst met integers
        Randvoorwaarden: gebruik een sorteerfunctie uit de module tentamen
                         de uitleg aan de hand van de O() waarom je voor dit
                         algoritme gekozen hebt staat in de desbetreffende string.
    '''
    uitleg = '''zet hier je uitleg'''

    #zet voor de return statement jouw code om de pseudocode in deze functie te
    #implementeren. Je mag de volgende regel vervangen/verwijderen.
    gesorteerde_lijst = lijstje

    return (uitleg, gesorteerde_lijst)


def vraag04():
    '''vraag 4.
            totaal 15 punten waarvan 5 voor de juiste datastructuur en
            maximaal 10 voor de uitleg.

    Met behulp van welke datastructuur in de module tentamen kun je
            efficiënt een binaire boom implementeren?
            Leg je antwoord uit.
            Je hoeft dus geen code te schrijven.
    '''
    datastructuur = 'vervang dit door de juiste datastructuur'
    uitleg = '''zet hier waarom je voor deze datastructuur kiest'''
    return (datastructuur, uitleg)


def vraag05(lijstje):
    '''vraag 5.
        Totaal 15 punten waarvan
             8 voor de keuze van de meest efficiënte datastructuur en algoritme
             7 voor werkende code (ook als dit niet het meest efficiënt is)

        Implementeer onderstaande pseudocode.

        Doel: keer de inhoud van een lijst met items om, bijvoorbeeld
            [1, 7, 2, 'aap'] wordt ['aap', 2, 7, 1]
        Input: een python lijst
        Output: dezelfde lijst, maar nu in omgekeerde volgorde
        Randvoorwaarden: Gebruik een datastructuur uit tentamen
    '''
    #zet voor de return statement jouw code om lijstje om te draaien

    return lijstje


def vraag06(lijstje):
    '''vraag 6. Implementeer onderstaande pseudocode
        maximaal aantal punten: 15
            waarvan 9 punten als de voor recursiviteit benodigde stappen aanwezig zijn
            6 punten als de code ook correct werkt

        Doel: bepaal RECURSIEF het grootste getal in een Python list
            en geef dit terug.
        Input: een Python list ('lijstje') met integers
        Output: de grootste integer uit lijstje
        Randvoorwaarden: de functie moet recursief zijn
    '''
    #schrijf hier je code, de print statement staat er alleen omdat pylint
    #anders puntenaftrek geeft. Deze statement mag je dus verwijderen.
    print(lijstje)


def vraag07():
    '''vraag 7.
        Totaal aantal punten maximaal 10, waarvan maximaal 7 voor de uitleg
        Bestudeer de functie hash_functie(). (zie onder)
        Is dit een goede functie om DNA-strings mee te hashen?
        Licht je antwoord toe.
    '''
    antwoord = ''
    toelichting = '''zet hier je toelichting'''
    return (antwoord, toelichting)


#====================================================================
#                      einde tentamenvragen
#     hier onder volgen de hulpfuncties voor vraag 3 en vraag 7
#====================================================================


def doeiets(getal):
    '''hulpfunctie voor vraag 03'''
    for i in range(0, getal):
        print('n' + str(i))


def functie01(l_met_ints):
    '''Analyseer de tijdscomplexteit van deze functie voor de lengte van de input
        input: lijst met integers l_met_ints
    '''
    integer = 5
    for teller in l_met_ints:
        print(l_met_ints[teller])
        doeiets(integer)


def hash_functie(dna_string):
    '''Doel: bereken een hash op basis van een string die DNA weergeeft
        Input: string die DNA weergeeft 'dna_string'
        Output: een numerieke hash
        Randvoorwaarden: de input string is gevalideerd en bevat alleen maar
            A's, C's, T's en G's.
    '''
    hashwaarde = 0
    for base in dna_string:
        hashwaarde = hashwaarde + int(base)
    return hashwaarde
