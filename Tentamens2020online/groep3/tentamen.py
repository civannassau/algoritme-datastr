#!/usr/bin/env python3

'''hulpcode voor gebruik bij het tentamen algoritmen en datastructuren 2020'''

def bubble_sort(lijst):
    '''an alternative implementation of the 'Bubble Sort' algorithm.
        Input: list containing integers
    '''
    lijst.sort()
    return lijst


def quick_sort(lijst):
    '''an alternative implementation of the 'Quick Sort' algorithm.
        Input: list containing integers
    '''
    lijst.sort()
    return lijst


class ArrayOfIntegers():
    '''a simple implementation of an array of integers'''

    def __init__(self, number):
        '''constructor, creates a new empty array of length number'''
        self._count = number
        self._data = [None for item in range(0, number)]

    def __len__(self):
        '''returns the maximum numbers of integers that can be contained in
            the array
        '''
        return len(self._data)

    def __getitem__(self, index):
        '''returns the item at index'''
        if index < 0:
            raise Exception('index should be > 0')
        else:
            return self._data[index]

    def __setitem__(self, index, value):
        '''sets value of array[index] to value'''
        if not isinstance(value, int):
            raise Exception('{0} is not an integer.'.format(value))
        elif index < 0:
            raise Exception('index should be > 0')
        else:
            if index > self._count:
                raise Exception('index > size array')
            self._data[index] = value


class MinHeap():
    '''implements a MinHeap'''

    def __init__(self):
        '''constructor, creates a new, empty heap'''
        self._data = []


    def add(self, value):
        '''adds an integer to the heap'''
        if not isinstance(value, int):
            raise Exception('{0} is not an integer.'.format(value))
        else:
            self._data.append(value)
            self._sift_up(len(self._data) -1)

    def get(self):
        '''returns the smallest item on the heap'''
        minimum = self._data[0]
        if len(self._data) > 1:
            self._data[0] = self._data.pop()
            self._sift_down(0)
        else:
            self._data = []
        return minimum


    def is_empty(self):
        '''returns True als de heap leeg is, anders False'''
        return not self._data


    def _sift_down(self, index):
        '''the sift down, requires the node to sift down'''
        left = 2 * index + 1
        right = 2 * index + 2
        smallest = index
        if (left < len(self._data) and
                self._data[left] < self._data[index]):
            smallest = left
        if (right < len(self._data) and
                self._data[right] < self._data[left]):
            smallest = right

        if smallest != index:
            temp = self._data[index]
            self._data[index] = self._data[smallest]
            self._data[smallest] = temp
            self._sift_down(smallest)

    def _sift_up(self, index):
        '''the sift up. requires node to sift up'''
        if index > 0:
            parent = index // 2
            if self._data[index] < self._data[parent]:
                temp = self._data[index]
                self._data[index] = self._data[parent]
                self._data[parent] = temp
                self._sift_up(parent)



class Stack():
    '''a simple implementation of a Stack'''

    def __init__(self):
        '''constructor, creates a new, empty stack'''
        self._data = []

    def push(self, item):
        '''adds a new item to the top of the stack. Requires an item, returns nothing'''
        self._data.append(item)

    def pop(self):
        '''removes and returns the top item of the stack'''
        return self._data.pop()

    def peek(self):
        '''returns the top item of the stack. The stack is not modified.'''
        return self._data[len(self._data) - 1]

    def is_empty(self):
        '''returns True if Stack is empty, returns False if not'''
        empty = True
        if self._data:
            empty = False
        return empty

    def size(self):
        '''returns the number of items on the stack'''
        return len(self._data)


class Queue():
    '''a simple implementation of a queue'''

    def __init__(self):
        '''creates a new, empty queue. No parameters required.'''
        self._data = []

    def enqueue(self, item):
        '''adds a new item to the rear of the queue. Requires the item, returns nothing.'''
        self._data.insert(0, item)

    def dequeue(self):
        '''removes and returns the item on the form of the queue. The queue is modified.'''
        return self._data.pop()

    def is_empty(self):
        '''returns True if Stack is empty, returns False if not'''
        empty = True
        if self._data:
            empty = False
        return empty

    def size(self):
        '''returns the number of items in the'''
        return len(self._data)


class Node():
    '''Nodes are used to create singly linked lists'''

    def __init__(self):
        '''constructor'''
        self._next_node = None
        self._value = ''


    def set_value(self, value):
        '''sets the value of the node
        input: integer
        '''
        if isinstance(value, int):
            self._value = value
        else:
            raise Exception('value is not an integer')


    def get_value(self):
        '''returns the value of the node'''
        return self._value


    def set_next_node(self, node):
        '''sets the next node'''
        if isinstance(node, Node):
            self._next_node = node
        else:
            raise Exception('node is not a Node')


    def get_next_node(self):
        '''returns the next node. If last node: returns None'''
        return self._next_node


class SinglyLinkedList():
    '''implements a very basic singly linked list.'''

    def __init__(self):
        '''constructor'''
        self._head = None
        self._current_node = None


    def get_first_node(self):
        '''returns the head of the list (= first node)'''
        self._current_node = self._head
        return self._head


    def append(self, node):
        '''appends node to the list
            input a Node
        '''
        current_node = self._head
        if current_node:
            while current_node.get_next_node():
                current_node = current_node.get_next_node()
            current_node.set_next_node(node)
        else:
            if isinstance(node, Node):
                self._head = node
            else:
                raise Exception('node is no Node')
